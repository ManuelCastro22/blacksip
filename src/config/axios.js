// Import axios
import axios from 'axios'
import qs from 'qs'


// Create axios client
const clientAxios = axios.create({
    // Use how a base the environment variable
    baseURL: process.env.REACT_APP_DATA_URL
})

export const clientAxiosPost = axios.create({
    // Use how a base the environment variable
    baseURL: process.env.REACT_APP_DATA_URL,
    headers: { 
        // 'Access-Control-Allow-Origin': '*',
        // 'Access-Control-Allow-Origin': 'http://localhost:3000',
        // 'Content-Type': 'application/json'
        'Content-Type': 'application/x-www-form-urlencoded'
    }   
})


export default clientAxios