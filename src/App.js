import React from 'react'
// Import components
import ShippingAddress from './components/ShippingAddress'
import OrderSummary from './components/OrderSummary'
// Import the states
import ShippingState from './context/shipping/ShippingState'
import OrderState from './context/order/OrderState'



function App() {
	return (
		<ShippingState>
			<OrderState>
				<div className="container container--general">
					<ShippingAddress></ShippingAddress>
					<OrderSummary></OrderSummary>
				</div>
			</OrderState>
		</ShippingState>
	)
}



export default App