// Import React and Hooks
import React, { useContext } from 'react'
// Import the Context shipping
import shippingContext from '../context/shipping/shippingContext'
// Import font-awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const ShippingAddress = ({ formInput }) => {

    // Extracting from the Context what is going to be used
    const shippingContextPure = useContext(shippingContext),
        { formData, updateFormData, getPostalCode } = shippingContextPure
    

    // Destructuring form input shipping
    const { idInput, value, iconName, placeholder, isDisabled, type, error, options } = formInput


    // Updating form state shipping
    const getDataInput = e => {
        if( e.target.name === 'postalCode' ) getPostalValue( e.target.value )
        updateFormData(e.target.name, e.target.value)
    }

    // Updating postal codes and its related
    const getPostalValue = postalCode => {
        if(postalCode.length >= 5) {
            getPostalCode(postalCode)
        }
    }

    // Create component type: input or select
    const inputEl = <input 
        type={type}
        className="form__input"
        placeholder={placeholder}
        name={idInput}
        id={idInput}
        value={value}
        disabled={ isDisabled ? true : false }
        onChange={ getDataInput }
    />

    const selectEl = <select
        className="form__select"
        placeholder={placeholder}
        name={idInput}
        id={idInput}
        value={value}
        onChange={ getDataInput }
        >
        {
            Array.isArray(options) && options.length > 1 && (
                options.map( (option, index) => ( <option value={option} key={index} > {option} </option>
                 ) )
            )
        }
    </select>

    return (
        <article className={`form__box ${ error === true ? 'form__box--error' : '' }`}>
            <label htmlFor={idInput} className="form__box__left">
                <FontAwesomeIcon icon={iconName} />
            </label>
            <div className="form__box__right">
                {
                    Array.isArray(options) && options.length > 1 ? selectEl : inputEl
                }
            </div>
        </article>
    )
}


export default ShippingAddress