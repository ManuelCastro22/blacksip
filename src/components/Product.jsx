import React from 'react'


const Product = ({ productCart }) => {

    // Destructuring product
    const { name, price, image } = productCart

    return (
        <article className="product">
            <div className="product__box">
                <img src={image} alt="" className="product__img"/>
            </div>
            <div className="product__box">
                <h3 className="product__name"> {name} </h3>
            </div>
            <div className="product__box">
                <h3 className="product__price">${price}.00</h3>
            </div>
        </article>
    )
}

 
export default Product