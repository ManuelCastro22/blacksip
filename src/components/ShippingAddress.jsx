import React, { Fragment } from 'react'
// Import components
import ShippingForm from './ShippingForm'


const ShippingAddress = () => {
    return (
        <Fragment>
            <div className="topBar">
                <h2 className="topBar__title">Dirección de envío</h2>
            </div>
            <ShippingForm />
        </Fragment>
    )
}
 
export default ShippingAddress