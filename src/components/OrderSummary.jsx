// Import React and Hooks
import React, { useContext, useEffect } from 'react'
// Import Components
import Product from './Product'
// Import the Context of Order
import orderContext from '../context/order/orderContext'


const OrderSummary = () => {

    // Extracting from the Context what is going to be used
    const orderContextPure = useContext(orderContext),
        { subtotal, total, productsCart, getProductsCart, getSubtotalCart, getTotalCart } = orderContextPure
    
    
    // When load the site, get the products cart
    useEffect( async () => {
        await getProductsCart()
        getSubtotalCart()
        getTotalCart()
    }, [])


    return (
        <section className="order pM0 mt60 mb60">
            <header className="order__header">
                <h2 className="order__title">Resumen de la orden</h2>
            </header>
            <div className="order__content">
                <div className="order__products">
                    {
                        productsCart.length > 0
                        ? 
                            productsCart.map( (productCart, index) => (
                                <Product productCart={productCart} key={index} />
                            )) 
                        : null
                    }
                </div>
                <div className="order__btns">
                    <button className="btn btn--invert mlA">Editar</button>
                </div>
            </div>
            <footer className="order__footer">
                <div className="order__subtotal">
                    <h2 className="order__subtotal__title">
                        Subtotal
                        <span className="order__subtotal__value">${subtotal}.00</span>
                    </h2>
                    <h2 className="order__subtotal__title">
                        Envío
                        <span className="order__subtotal__value">A calcular</span>
                    </h2>
                </div>
                <div className="order__total">
                    <h2 className="order__total__title">
                        Total
                        <span className="order__total__value">${total}.00</span>
                    </h2>
                </div>
            </footer>
        </section>
    )
}
 

export default OrderSummary