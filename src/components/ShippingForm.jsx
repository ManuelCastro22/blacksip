// Import React and Hooks
import React, { useContext, useState } from 'react'
// Import components
import ShippingInput from './ShippingInput'
import Spinner from './Spinner'
// Import the Context shipping
import shippingContext from '../context/shipping/shippingContext'


const ShippingAddress = () => {

    // Extracting from the Context what is going to be used
    const shippingContextPure = useContext(shippingContext),
        { formData, completedForm, errorForm, showLoader, validateErrorsForm, isCompleteForm, sendDataForm } = shippingContextPure
    

    // Submit Form
    const submitForm = e => {

        e.preventDefault()

        
        // Validation error of inputs
        validateErrorsForm()


        // Validate all form
        isCompleteForm()
        if( completedForm === false || completedForm === null ) return

        // Send Data
        sendDataForm()

    }


    return (
        <form 
            className="form"
            noValidate
            onSubmit={ submitForm }>
            <div className="form__container pt80 ptM35">
                {
                    formData
                    ?
                        formData.map( (formInput, index) => (
                            <ShippingInput
                                formInput={formInput}
                                key={index} />
                         ))
                    : null
                }
                <div className="form__btns">
                    <button 
                        type="button"
                        className="btn">
                        Libreta de direcciones
                    </button>
                    <button 
                        type="submit"
                        className="btn">
                        Guardar
                    </button>
                </div>
                <article className="form__box form__box--checkbox mt10">
                    <input type="checkbox" className="form__checkbox" id="useIdAddress" />
                    <label htmlFor="useIdAddress" className="form__label form__label--checkbox">Utilizar como dirección de facturación.</label>
                </article>
            </div>
            {
                showLoader && (
                    <Spinner />
                )
            }
            { 
                errorForm && (
                <div className="form__alert form__alert--success">
                    <h2 className="form__alert__title">Enhorabuena!</h2>
                    <p className="form__alert__text">Los datos fueron enviados</p>
                </div>
                )
            }
        </form>
    )
}


export default ShippingAddress