// Import the types
import {
    GET_PRODUCTS_CART,
    GET_SUBTOTAL_CART,
    GET_TOTAL_CART
    } from '../../types/index'


export default ( state, action ) => {
    switch( action.type ){
        case GET_PRODUCTS_CART:
            return {
                ...state,
                productsCart: action.payload
            }
        case GET_SUBTOTAL_CART:
            return {
                ...state,
                subtotal: state.productsCart.reduce( (currentPrice, product ) => { return currentPrice + parseInt(product.price)}, 0 )
            }
        case GET_TOTAL_CART:
            return {
                ...state,
                total: state.productsCart.reduce( (currentPrice, product ) => { return currentPrice + parseInt(product.price)}, 0 )
            }
        default:
            return state
    }
}