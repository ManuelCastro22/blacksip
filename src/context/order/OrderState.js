// Import React and hooks
import React, { useReducer } from 'react'
// Import the context and reducer of order
import orderContext from './orderContext'
import orderReducer from './orderReducer'
// Import types
import {
    GET_PRODUCTS_CART,
    GET_SUBTOTAL_CART,
    GET_TOTAL_CART
    } from '../../types/index'
// Import function axios
import clientAxios from '../../config/axios'


const OrderState = props => {

    // Initial state
    const initialState = {
        productsCart: [],
        subtotal: 0,
        total: 0
    }


    // Dispatch for execute the actions
    const [ state, dispatch ] = useReducer(orderReducer, initialState)


    // Get Products Cart
    const getProductsCart = async () => {

        try {
            
            // Send request
            const response = await clientAxios.get('/products')

            dispatch({
                type: GET_PRODUCTS_CART,
                payload: response.data
            })

        } catch(error) {
            console.log(error)
        }

    }

    // Get Subtotal Cart
    const getSubtotalCart = () => {

        dispatch({
            type: GET_SUBTOTAL_CART
        })

    }

    // Get Total Cart
    const getTotalCart = () => {

        dispatch({
            type: GET_TOTAL_CART
        })

    }


    return (
        <orderContext.Provider
            value={{
                productsCart: state.productsCart,
                subtotal: state.subtotal,
                total: state.total,
                getProductsCart,
                getSubtotalCart,
                getTotalCart
            }}>
            { props.children }
        </orderContext.Provider>
    )

}
 
export default OrderState