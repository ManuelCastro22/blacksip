// Import for create the context
import { createContext } from 'react'


// Create the context shipping
const shippingContext = createContext()


export default shippingContext