// Import React and hooks
import React, { useReducer } from 'react'
// Import font-awesome
import { 
    faUser,
    faEnvelope,
    faPhoneAlt,
    faMapMarkerAlt,
    faMapMarkedAlt
} from '@fortawesome/free-solid-svg-icons'
// Import the context and reducer of shipping
import shippingContext from './shippingContext'
import shippingReducer from './shippingReducer'
// Import types
import {
    UPDATE_FORM_DATA,
    VALIDATE_ERRORS_FORM,
    GET_POSTAL_CODE,
    IS_COMPLETED_FORM,
    SEND_FORM,
    CHANGE_ERROR,
    SHOW_LOADER
    } from '../../types/index'
// Import function axios
import clientAxios, {clientAxiosPost} from '../../config/axios'
// Import for convert to json
import qs from 'qs'

const ShippingState = props => {

    // Initial state
    const initialState = {
        formData: [
            {
                idInput: 'name',
                type: 'text',
                value: '',
                iconName: faUser,
                placeholder: 'Nombre',
                isDisabled: false,
                options: [],
                error: null
            },
            {
                idInput: 'lastname',
                value: '',
                type: 'text',
                iconName: faUser,
                placeholder: 'Apellido',
                isDisabled: false,
                options: [],
                error: null
            },
            {
                idInput: 'email',
                value: '',
                type: 'email',
                iconName: faEnvelope,
                placeholder: 'Correo Electrónico',
                isDisabled: false,
                options: [],
                error: null
            },
            {
                idInput: 'phone',
                value: '',
                type: 'number',
                iconName: faPhoneAlt,
                placeholder: 'Número de teléfono',
                isDisabled: false,
                options: [],
                error: null
            },
            {
                idInput: 'postalCode',
                value: '',
                type: 'number',
                iconName: faMapMarkerAlt,
                placeholder: 'Código postal',
                isDisabled: false,
                options: [],
                error: null 
            },
            {
                idInput: 'colonies',
                value: '',
                type: 'text',
                iconName: faMapMarkerAlt,
                placeholder: 'Colonia',
                isDisabled: true,
                options: [],
                error: null
            },
            {
                idInput: 'state',
                value: '',
                type: 'text',
                iconName: faMapMarkerAlt,
                placeholder: 'Estado/Región',
                isDisabled: true,
                options: [],
                error: null
            },
            {
                idInput: 'city',
                value: '',
                type: 'text',
                iconName: faMapMarkerAlt,
                placeholder: 'Ciudad',
                isDisabled: true,
                options: [],
                error: null
            },
            {
                idInput: 'town',
                value: '',
                type: 'text',
                iconName: faMapMarkerAlt,
                placeholder: 'Delegación o municipio',
                isDisabled: true,
                options: [],
                error: null
            },
            {
                idInput: 'street',
                value: '',
                type: 'text',
                iconName: faMapMarkedAlt,
                placeholder: 'Calle',
                isDisabled: false,
                options: [],
                error: null
            }
        ],
        dataToSend: [],
        completedForm: null,
        errorForm: null,
        showLoader: null
    }


    // Dispatch for execute the actions
    const [ state, dispatch ] = useReducer(shippingReducer, initialState)


    // Update form data
    const updateFormData = (inputName, inputValue) => {
        dispatch({
            type: UPDATE_FORM_DATA,
            payload: {inputName, inputValue}
        })
    }

    // Validate input forms
    const validateErrorsForm = () => {
        dispatch({
            type: VALIDATE_ERRORS_FORM
        })
    }

    // Get postal code
    const getPostalCode = async postalCode => {
        
        try {

            // Send request
            const response = await clientAxios.get(`/postalCodes/${postalCode}`)

            let dataPostalCode

            if( Object.keys(response.data).length > 0 ) {
                dataPostalCode = response.data
            } else {
                dataPostalCode = {
                    city: '-',
                    colonies: '-',
                    state: '-',
                    town: '-',
                    noOptions: ''
                }
            }

            dispatch({
                type: GET_POSTAL_CODE,
                payload: dataPostalCode
            })

        } catch(error){
            console.log(error)
        }

    }

    // is completed form
    const isCompleteForm = () => {
        dispatch({
            type: IS_COMPLETED_FORM
        })
    }

    // Send data form
    const sendDataForm = async () => {

        dispatch({
            type: SEND_FORM
        })

        try {

            dispatch({
                type: SHOW_LOADER,
                payload: true
            })

            // const response = await clientAxiosPost.post(`/contact`, qs.stringify(state.dataToSend))
            const response = await clientAxiosPost.post(`/contact`, state.dataToSend)
            
        } catch(error) {

            dispatch({
                type: SHOW_LOADER,
                payload: false
            })

            dispatch({
                type: CHANGE_ERROR,
                payload: true
            })
            
            setTimeout( () => {
                dispatch({
                    type: CHANGE_ERROR,
                    payload: false
                })
            }, 3000 )

        }

    }

    return (
        <shippingContext.Provider
            value={{
                formData: state.formData,
                dataToSend: state.dataToSend,
                completedForm: state.completedForm,
                errorForm: state.errorForm,
                showLoader: state.showLoader,
                updateFormData,
                validateErrorsForm,
                getPostalCode,
                isCompleteForm,
                sendDataForm
            }}>
            { props.children }
        </shippingContext.Provider>
    )

}
 
export default ShippingState