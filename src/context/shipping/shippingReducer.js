// Import the types
import {
    UPDATE_FORM_DATA,
    VALIDATE_ERRORS_FORM,
    GET_POSTAL_CODE,
    IS_COMPLETED_FORM,
    SEND_FORM,
    CHANGE_ERROR,
    SHOW_LOADER
    } from '../../types/index'


export default ( state, action ) => {
    switch( action.type ){
        case UPDATE_FORM_DATA:
            return {
                ...state,
                formData: state.formData.map( formI => {
                    // Update value of each input
                    if( formI.idInput === action.payload.inputName ){
                        formI.value = action.payload.inputValue
                    }
                    return formI
                } )
            }
        case VALIDATE_ERRORS_FORM:
            const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
            return {
                ...state,
                formData: state.formData.map( formI => {
                    // Validate than the inputs no are empty
                    formI.error = formI.value.length < 1 ? true : false
                    // Validate email by regex
                    if( formI.type === 'email' ){
                        formI.error = emailPattern.test(formI.value) ? false : true
                    }
                    return formI
                } )
            }
        case GET_POSTAL_CODE:
            return {
                ...state,
                formData: state.formData.map( formI => {
                    if( action.payload.noOptions ) formI.options = ''
                    if( formI.idInput === 'city' ) formI.value = action.payload.city
                    if( formI.idInput === 'colonies' ){ 
                        formI.options = action.payload.colonies
                        formI.value = action.payload.colonies[0]
                    }
                    if( formI.idInput === 'state' ) formI.value = action.payload.state
                    if( formI.idInput === 'town' ) formI.value = action.payload.town
                    return formI
                } )
            }
        case IS_COMPLETED_FORM:
            return {
                ...state,
                completedForm: state.formData.every( formI => formI.error === false )
            }
        case SEND_FORM:
            return {
                ...state,
                dataToSend: state.formData.map( formI => {
                    return {
                        name: formI.idInput,
                        value: formI.value
                    }
                } )
            }
        case CHANGE_ERROR:
            return {
                ...state,
                errorForm: action.payload
            }
        case SHOW_LOADER:
            return {
                ...state,
                showLoader: action.payload
            }
        default:
            return state
    }
}