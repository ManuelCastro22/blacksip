// Actions of the application

// Shipping
export const UPDATE_FORM_DATA = 'UPDATE_FORM_DATA'
export const VALIDATE_ERRORS_FORM = 'VALIDATE_ERRORS_FORM'
export const GET_POSTAL_CODE = 'GET_POSTAL_CODE'
export const IS_COMPLETED_FORM = 'IS_COMPLETED_FORM'
export const SEND_FORM = 'SEND_FORM'
export const CHANGE_ERROR = 'CHANGE_ERROR'
export const SHOW_LOADER = 'SHOW_LOADER'


// Order
export const GET_PRODUCTS_CART = 'GET_PRODUCTS_CART'
export const GET_SUBTOTAL_CART = 'GET_SUBTOTAL_CART'
export const GET_TOTAL_CART = 'GET_TOTAL_CART'